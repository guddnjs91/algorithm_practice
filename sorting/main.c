#include <stdio.h>
#include <stdlib.h>
#define SORTING
//#define LARGE_TEST

// TODO: modify this function to sort given array faster!
// example: {1, 5, 2, 4, 3} -> {1, 2, 3, 4, 5}
void sortArr(int *arr, int size) 
{
    int i, j;

    for (i = size; i > 0; i--) {
        for (j = 0; j < i-1; j++) {
            if (arr[j] > arr[j+1]) {
                int temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;
            }
        }
    }
}

/*************************************************
 * NOTE: Don't modify below code !!! *
 ************************************************/
// print all elements in array
void printArr(int *arr, int n) 
{
    int i;
    for (i = 0; i < n; i++) {
        printf("%d\n", arr[i]);
    }
}

int main(void)
{
    int *arr;
    int size;
    FILE* inputFile;
    FILE* outputFile;
    int i = 0;

    // read input file
#ifdef LARGE_TEST
    inputFile = fopen("input-large.txt", "r");
#else
    inputFile = fopen("input-small.txt", "r");
#endif
    if (inputFile == NULL) {
        printf("fopen error\n");
    }

    fscanf(inputFile, "%d", &size);
    printf("size = %d\n", size);
    arr = (int *) malloc(sizeof(int)*size);
    while (i < size) {
        fscanf(inputFile, "%d", &arr[i]);
        i++;
    }

    // Sorting begin
    sortArr(arr, size);
#ifndef LARGE_TEST
    printArr(arr, size);
#endif

    // write output file
    outputFile = fopen("output.txt", "w");
    if (outputFile == NULL) {
        printf("fopen error\n");
    }

    fprintf(outputFile, "%d\n", size);
    for (i = 0; i < size; i++) {
        fprintf(outputFile, "%d\n", arr[i]);
    }

    return 0;
}
